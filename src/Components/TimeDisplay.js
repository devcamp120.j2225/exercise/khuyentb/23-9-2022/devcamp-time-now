import { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css"



class TimeDisplay extends Component {
    constructor(props) {
        super(props);

        let today = new Date().toLocaleTimeString();
        
        this.state = {
            currentTime: today,
            second: new Date().getSeconds(),
            btnColor: "black"
            
        }
    }

    buttonClickChangeColorHandler = () => {
        if (this.state.second % 2 === 0) {
            this.setState({
                btnColor: "red"
            })
        } else {
            this.setState({
                btnColor: "blue"
            })
        }
    }
    render () {
        return (
            <div className="container text-center mt-5" style={{backgroundColor: "#CCF1FF", padding: "10px", width: "600px", border: "7px solid"}}>
                <h1 style={{color: this.state.btnColor}}>Hello, world!</h1>
                <p>It is {this.state.currentTime}</p>            
                <button className="btn btn-success col-3" onClick={this.buttonClickChangeColorHandler}>Click me!</button>                          
            </div>
        ) 
    }
}

export default TimeDisplay;